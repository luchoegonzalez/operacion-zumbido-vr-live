%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: MosqComunCuerpo
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.L
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.L/PATA1.L
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.L/PATA1.L/PATA2.L
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.L/PATA1.L/PATA2.L/PATA3.L
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.L/PATA1.L/PATA2.L/PATA3.L/PATA3.L_end
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.R
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.R/PATA1.R
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.R/PATA1.R/PATA2.R
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.R/PATA1.R/PATA2.R/PATA3.R
    m_Weight: 1
  - m_Path: Armature/ANTEPATA.R/PATA1.R/PATA2.R/PATA3.R/PATA3.R_end
    m_Weight: 1
  - m_Path: Armature/cola
    m_Weight: 1
  - m_Path: Armature/cola/cola_end
    m_Weight: 1
  - m_Path: Armature/torso.bajo
    m_Weight: 1
  - m_Path: Armature/torso.bajo/anteala.L
    m_Weight: 1
  - m_Path: Armature/torso.bajo/anteala.L/ala.L
    m_Weight: 0
  - m_Path: Armature/torso.bajo/anteala.L/ala.L/ala.L_end
    m_Weight: 1
  - m_Path: Armature/torso.bajo/anteala.R
    m_Weight: 1
  - m_Path: Armature/torso.bajo/anteala.R/ala.R
    m_Weight: 0
  - m_Path: Armature/torso.bajo/anteala.R/ala.R/ala.R_end
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOALTO1.L
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOALTO1.L/BRAZOALTO2.L
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOALTO1.L/BRAZOALTO2.L/BRAZOALTO3.L
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOALTO1.L/BRAZOALTO2.L/BRAZOALTO3.L/BRAZOALTO3.L_end
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOALTO1.R
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOALTO1.R/BRAZOALTO2.R
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOALTO1.R/BRAZOALTO2.R/BRAZOALTO3.R
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOALTO1.R/BRAZOALTO2.R/BRAZOALTO3.R/BRAZOALTO3.R_end
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOMEDIO1.L
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOMEDIO1.L/BRAZOMEDIO2.L
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOMEDIO1.L/BRAZOMEDIO2.L/BRAZOMEDIO3.L
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOMEDIO1.L/BRAZOMEDIO2.L/BRAZOMEDIO3.L/BRAZOMEDIO3.L_end
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOMEDIO1.R
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOMEDIO1.R/BRAZOMEDIO2.R
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOMEDIO1.R/BRAZOMEDIO2.R/BRAZOMEDIO3.R
    m_Weight: 1
  - m_Path: Armature/torso.bajo/BRAZOMEDIO1.R/BRAZOMEDIO2.R/BRAZOMEDIO3.R/BRAZOMEDIO3.R_end
    m_Weight: 1
  - m_Path: Armature/torso.bajo/torso.alto
    m_Weight: 1
  - m_Path: Armature/torso.bajo/torso.alto/cabeza
    m_Weight: 1
  - m_Path: Armature/torso.bajo/torso.alto/cabeza/pico1
    m_Weight: 1
  - m_Path: Armature/torso.bajo/torso.alto/cabeza/pico1/pico2
    m_Weight: 1
  - m_Path: Armature/torso.bajo/torso.alto/cabeza/pico1/pico2/pico2_end
    m_Weight: 1
  - m_Path: Cube
    m_Weight: 1
  - m_Path: Cube.001
    m_Weight: 1
  - m_Path: Cube.002
    m_Weight: 1
  - m_Path: Cube.003
    m_Weight: 1
  - m_Path: Cube.004
    m_Weight: 1
  - m_Path: Cube.005
    m_Weight: 1
  - m_Path: Cube.006
    m_Weight: 1
  - m_Path: Cube.007
    m_Weight: 1
  - m_Path: Cube.008
    m_Weight: 1
  - m_Path: Cube.009
    m_Weight: 1
  - m_Path: Cube.010
    m_Weight: 1
  - m_Path: Cube.011
    m_Weight: 1
  - m_Path: Cube.012
    m_Weight: 1
  - m_Path: Cube.013
    m_Weight: 1
  - m_Path: Cylinder
    m_Weight: 1
  - m_Path: NurbsPath
    m_Weight: 1
  - m_Path: NurbsPath.001
    m_Weight: 1
  - m_Path: Sphere
    m_Weight: 1
  - m_Path: Sphere.001
    m_Weight: 1
  - m_Path: Sphere.002
    m_Weight: 1
  - m_Path: Torus.001
    m_Weight: 1
