using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MosquitoComunControl : MonoBehaviour
{
    public float rapidez = 2;
    public float distanciaDeAtaque;
    Animator animatorMosquito;
    GameManager scriptGameManager;
    GameObject jugadorDestino;

    public string elemento;

    bool enPosicionDeAtaque = false;

    AudioSource sonidoAtaque;
    public AudioClip sonidoMuerte;

    void Start()
    {
        scriptGameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        animatorMosquito = GetComponent<Animator>();
        jugadorDestino = scriptGameManager.mainCamera.transform.GetChild(0).gameObject;
        sonidoAtaque = transform.GetChild(1).GetComponent<AudioSource>();
    }

    void LateUpdate()
    {
        float distanciaDeJugador = Vector3.Distance(transform.position, jugadorDestino.transform.position);
        transform.LookAt(jugadorDestino.transform);

        if (distanciaDeJugador > distanciaDeAtaque && !enPosicionDeAtaque)
        {
            transform.Translate((Vector3.forward * rapidez * Time.deltaTime) + (Vector3.down * 2 * Time.deltaTime));

        } else
        {
            transform.SetParent(scriptGameManager.objetoSeguidorCaida.transform);

            if (!enPosicionDeAtaque)
            {
                enPosicionDeAtaque = true;
                Atacar();
            }
        }
    }

    void Atacar()
    {
        animatorMosquito.SetTrigger("Picar");
        Invoke("Recuperarse", 3f);
    }

    public void HacerDanoJugador()
    {
        scriptGameManager.PerderVidaJugador(1);
        sonidoAtaque.Play();
    }

    void Recuperarse()
    {
        animatorMosquito.SetTrigger("Recuperar");
        Invoke("Atacar", 3f);
    }

    void OnCollisionEnter(Collision colision)
    {
        if (colision.gameObject.CompareTag("balaFuego"))
        {
            if (elemento == "hielo" || elemento == "normal") 
            {
                Morir();
                PowerUpControl.contPowerUpFuego++;
            }
        }

        if (colision.gameObject.CompareTag("balaAgua"))
        {
            if (elemento == "fuego" || elemento == "normal")
            {
                Morir();
                PowerUpControl.contPowerUpAgua++;
            }
        }

        if (colision.gameObject.CompareTag("espada"))
        {
            Morir();
        }
    }

    void Morir()
    {
        Destroy(gameObject, 3.5f);
        animatorMosquito.SetBool("Muerto", true);
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        transform.GetChild(0).GetComponent<AudioSource>().Stop();
        transform.GetChild(0).GetComponent<AudioSource>().PlayOneShot(sonidoMuerte);
        transform.GetChild(0).GetComponent<AudioSource>().loop = false;
        GetComponent<BoxCollider>().isTrigger = true;
        transform.parent = null;
    }

    //void OnTriggerEnter(Collider col)
    //{
    //    if (col.CompareTag("espada"))
    //    {
    //        Morir();
    //    }
    //}
}
