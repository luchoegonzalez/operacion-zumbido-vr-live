using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlesMosquito : MonoBehaviour
{
    Animator animatorMosquito;

    private void Start()
    {
        animatorMosquito = GetComponent<Animator>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("balaAgua") || collision.gameObject.CompareTag("balaFuego"))
        {
            Morir();
        }
    }
    void Morir()
    {
        Invoke("Renacer", 2f);
        animatorMosquito.SetBool("Renacer", false);
        animatorMosquito.SetBool("Muerto", true);
    }

    void Renacer()
    {
        animatorMosquito.SetBool("Muerto", false);
        animatorMosquito.SetBool("Renacer", true);
    }
}
