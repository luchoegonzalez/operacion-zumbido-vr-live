using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinVRControlJugador : MonoBehaviour
{
    Vector2 mouseMirar;
    Vector2 suavidadV;
    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;
    public float rapidezDesplazamiento = 10.0f;
    public ManosControl scriptManoDerecha;
    public ManosControl scriptManoIzquierda;

    GameObject jugador;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        jugador = this.transform.parent.gameObject;
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetMouseButtonDown(0))
        {
            scriptManoIzquierda.Disparar();
        }
        if (Input.GetMouseButtonDown(1))
        {
            scriptManoDerecha.Disparar();
        }

        if (Input.GetKeyDown("r"))
        {
            scriptManoDerecha.Recargar();
            scriptManoIzquierda.Recargar();
        }
    }

    private void LateUpdate()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));
        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);
        mouseMirar += suavidadV; mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90f, 90f);
        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, jugador.transform.up);
    }
}
