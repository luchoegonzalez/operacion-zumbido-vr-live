using UnityEngine;

public class JugadorControl : MonoBehaviour
{
    public static bool saltando = false;
    public GameObject manoDerecha;
    public bool habilitarSalto = false;
    bool enHelicoptero = true;

    public GameObject cabeza;
    public Animator helicopteroAnimator;

    public GameManager scriptGameManager;
    WavesControl scriptWavesControl;

    public AudioSource sonidoViento;
    public AudioSource musica;

    public GameObject instruccionAsentir;

    private void Start()
    {
        scriptWavesControl = scriptGameManager.gameObject.GetComponent<WavesControl>();
    }

    // Update is called once per frame
    void Update()
    {
        if (cabeza.transform.rotation.eulerAngles.x <= 357 && cabeza.transform.rotation.eulerAngles.x >= 270 && enHelicoptero)
        {
            habilitarSalto = true;
        }
        if (habilitarSalto && enHelicoptero && cabeza.transform.rotation.eulerAngles.x <= 90 && cabeza.transform.rotation.eulerAngles.x >= 5f)
        {
            helicopteroAnimator.SetTrigger("inicioSalto");
            Invoke("Saltar", 5f);
            habilitarSalto = false;
            enHelicoptero = false;
            instruccionAsentir.SetActive(false);
        }
    }

    private void Saltar()
    {
        saltando = true;
        //scriptGameManager.ComenzarSpawnMosquitos();
        Invoke("ComenzarOleadas", 10f);
        sonidoViento.PlayDelayed(1);
        musica.PlayDelayed(3);
    }

    void ComenzarOleadas()
    {
        StartCoroutine(scriptWavesControl.SpawnWave());
    }
}
