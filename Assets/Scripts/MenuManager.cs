using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Transform menuObjeto;
    public Transform posicionMenu;
    public Transform camara;
    public float suavidadMenu = 0.5f;

    private void Update()
    {
        menuObjeto.position = Vector3.Lerp(menuObjeto.position, new Vector3(posicionMenu.position.x, camara.position.y - 0.1f, posicionMenu.position.z), suavidadMenu);
        //menuObjeto.position = new Vector3(posicionMenu.position.x, camara.position.y - 0.1f, posicionMenu.position.z);
        //Vector3 rotacionSuave = Vector3.Lerp(menuObjeto.rotation.eulerAngles, new Vector3(0, camara.rotation.eulerAngles.y, 0), suavidadMenu);
        //menuObjeto.rotation = Quaternion.Euler(rotacionSuave);
        menuObjeto.rotation = Quaternion.Lerp(menuObjeto.rotation, Quaternion.Euler(new Vector3(0, camara.rotation.eulerAngles.y, 0)), suavidadMenu);
    }
}
