using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpControl : MonoBehaviour
{
    static public int contPowerUpFuego = 0;
    static public int contPowerUpAgua = 0;

    public ManosControl manoDeAgua;
    public Material indicadorPowerupAgua;
    public ManosControl manoDeFuego;
    public Material indicadorPowerupFuego;

    public void Update ()
    {

        indicadorPowerupAgua.SetFloat("_Carga", 0.01f * contPowerUpAgua * 3.1f);
        indicadorPowerupFuego.SetFloat("_Carga", 0.01f * contPowerUpFuego * 3.1f);

        if (contPowerUpAgua >= 7)
        {
            manoDeAgua.powerUpHabilitado = true;
        }

        if (contPowerUpFuego >= 7)
        {
            manoDeFuego.powerUpHabilitado = true;
        }
    }
}
