using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    //public GameObject mosquitoNormalPrefab;
    //public GameObject mosquitoHieloPrefab;
    //public GameObject mosquitoFuegoPrefab;
    public GameObject jugador;
    //public GameObject destinoJugador;
    //public float frecuenciaSpawnMosquitos = 10.0f;
    public GameObject objetoSeguidorCaida;
    public Vector3 offset;
    public GameObject mainCamera;

    public int vidaJugador = 10;

    //public List<GameObject> listaPrefabMosquitos;
    //public float distanciaMinimaSpawnMosquitos = 10;

    public GameObject balasVisual1, balasVisual2;

    public Material materialVida;
    public GameObject gameOverPantalla;
    public TMP_Text textoSegundosVivo;
    float segundosVivo;

    public Animator generalAnimator;
    public AudioSource audioGeneral;
    float timerGeneral = 0;

    WavesControl scriptControlOleadas;

    public AudioSource musica;
    public AudioClip sonidoGameOver;

    public GameObject instruccionAsentir;

    void Start()
    {
        offset = objetoSeguidorCaida.transform.position - jugador.transform.position;
        scriptControlOleadas = GetComponent<WavesControl>();
        //listaPrefabMosquitos = new List<GameObject> { mosquitoNormalPrefab, mosquitoHieloPrefab, mosquitoFuegoPrefab };
        GraficarVida();

        Invoke("AnimarSoldado", 5f);
        Invoke("ActivarInstruccion", 5f);
    }

    void Update()
    {
        //objetoSeguidorCaida.transform.position = jugador.transform.position + offset;
        if (JugadorControl.saltando && vidaJugador > 0)
        {
            jugador.transform.Translate(Vector3.down * 2 * Time.deltaTime);
            segundosVivo += Time.deltaTime;
        }

        if (JugadorControl.saltando)
        {
            CancelInvoke("ActivarInstruccion");
            instruccionAsentir.SetActive(false);
        }

        timerGeneral += Time.deltaTime;
        if (timerGeneral >= 15f && !JugadorControl.saltando)
        {
            AnimarSoldado();
        }
    }

    private void LateUpdate()
    {
        objetoSeguidorCaida.transform.position = mainCamera.transform.position - offset;
        //objetoSeguidorCaida.transform.position = Vector3.Lerp(objetoSeguidorCaida.transform.position, mainCamera.transform.position - offset, 5 * Time.deltaTime);
    }

    //public void ComenzarSpawnMosquitos()
    //{
    //    InvokeRepeating("SpawnMosquitosComunes", 2.0f, frecuenciaSpawnMosquitos);
    //}

    //void SpawnMosquitosComunes()
    //{
    //    //Vector3 PosicionDeAparicionRandom = new Vector3(jugador.transform.position.x + Random.Range(-15, 15), jugador.transform.position.y, jugador.transform.position.z + Random.Range(-15, 15));
    //    int indiceAleatorio = Random.Range(0, listaPrefabMosquitos.Count);
    //    Instantiate(listaPrefabMosquitos[indiceAleatorio], PosicionRadomSpawn(), Quaternion.identity);
    //}

    void GameOver()
    {
        Time.timeScale = 0;
        gameOverPantalla.SetActive(true);
        balasVisual1.SetActive(false);
        balasVisual2.SetActive(false);

        musica.Stop();
        musica.PlayOneShot(sonidoGameOver);
        OcultarMosquitos();
        GraficarTiempoVivo();
        StartCoroutine(EjecutarDespuesDeSegundos(10f, ReiniciarJuego));
    }

    void OcultarMosquitos()
    {
        GameObject[] mosquitos = GameObject.FindGameObjectsWithTag("mosquito");
        foreach (GameObject mosquito in mosquitos)
        {
            mosquito.SetActive(false);
        }
    }

    private IEnumerator EjecutarDespuesDeSegundos(float seconds, System.Action method)
    {
        yield return new WaitForSecondsRealtime(seconds);
        method();
    }

    void ReiniciarJuego()
    {
        Time.timeScale = 1;
        JugadorControl.saltando = false;
        PowerUpControl.contPowerUpFuego = 0;
        PowerUpControl.contPowerUpAgua = 0;
        SceneManager.LoadScene(0);
    }

    public void PerderVidaJugador(int dano)
    {
        vidaJugador -= dano;
        GraficarVida();
        if (vidaJugador <= 0)
        {
            GameOver();
        }
    }

    void GraficarVida()
    {
        materialVida.SetFloat("_Carga", 0.01f * vidaJugador * 2.2f);
    }

    void GraficarTiempoVivo()
    {
        int min = Mathf.FloorToInt(segundosVivo / 60F);
        int seg = Mathf.FloorToInt(segundosVivo % 60F);
        textoSegundosVivo.text = "Has sobrevivido " + scriptControlOleadas.cantidadOleadas.ToString() + " oleadas y " + string.Format("{0:0}:{1:00}", min, seg) + " minutos";
    }

    void AnimarSoldado()
    {
        generalAnimator.SetTrigger("Saludar");
        audioGeneral.Play();
        timerGeneral = 0;
    }

    void ActivarInstruccion()
    {
        instruccionAsentir.SetActive(true);
    }
}
