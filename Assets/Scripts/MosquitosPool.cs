using System.Collections.Generic;
using UnityEngine;

public class MosquitosPool : MonoBehaviour
{
    public GameObject prefabEnemigo;
    public int cantidadInicial = 5;

    private List<GameObject> pool;

    void Start()
    {
        pool = new List<GameObject>();

        for (int i = 0; i < cantidadInicial; i++)
        {
            GameObject enemigo = Instantiate(prefabEnemigo);
            enemigo.SetActive(false);
            pool.Add(enemigo);
        }
    }

    public GameObject ObtenerEnemigo()
    {
        
        foreach (GameObject enemigo in pool)
        {
            if (!enemigo.activeInHierarchy)
            {
                enemigo.SetActive(true); 
                return enemigo;
            }
        }

        GameObject nuevoEnemigo = Instantiate(prefabEnemigo);
        pool.Add(nuevoEnemigo);
        return nuevoEnemigo;
    }

    public void DevolverEnemigo(GameObject enemigo)
    {
        enemigo.SetActive(false);
    }
}