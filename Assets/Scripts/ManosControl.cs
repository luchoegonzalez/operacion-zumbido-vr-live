using UnityEngine;
using TMPro;
using UnityEditor;

public class ManosControl : MonoBehaviour
{
    public GameObject balaFuego;
    public GameObject balaAgua;
    public GameObject puntoInstanciadorDeBalas;
    public float fuerzaProyectil;
    public TMP_Text indicadorBalas;
    //public GameObject indicadorVida;
    public GameObject espada;
    public GameObject mainCamera;

    bool modoPistola = false;
    bool permitirRecarga = true;
    bool bloqueoDeGestos = false;
    int balasDisponibles = 20;
    GameObject balaPrefab;

    public bool powerUpHabilitado = false;
    bool powerUpActivo = false;

    private Material material;

    public AudioClip sonidoPowerUp;
    public AudioClip sonidoDisparo;
    public AudioClip sonidoReload;
    private AudioSource audioFuente;
    private AudioSource audioPowerup;
    public AudioClip sonidoPowerActivo;
    bool powerupHaSonado;

    public enum OpcionesDropdown
    {
        Fuego,
        Agua,
    }

    public OpcionesDropdown tipoDeElemento;

    private void Start()
    {
        material = transform.GetChild(1).GetComponent<Renderer>().material;
        indicadorBalas.text = balasDisponibles.ToString();
        audioFuente = GetComponent<AudioSource>();
        audioPowerup = transform.GetChild(0).GetComponent<AudioSource>();

        if (tipoDeElemento == OpcionesDropdown.Fuego)
        {
            balaPrefab = balaFuego;
        }
        else if (tipoDeElemento == OpcionesDropdown.Agua)
        {
            balaPrefab = balaAgua;
        }
    }

    private void Update()
    {
        if (powerUpHabilitado)
        {
            HacerSonidoPowerup();
            material.SetFloat("_rapidezAnimacion", 3);
            material.SetInt("_powerupHabilitado", 1);
        } else
        {
            material.SetFloat("_rapidezAnimacion", 0);
            material.SetInt("_powerupHabilitado", 0);
        }
    }

    void HacerSonidoPowerup()
    {
        if (!powerupHaSonado)
        {
            audioPowerup.PlayOneShot(sonidoPowerUp);
            powerupHaSonado = true;
        }
    }

    private void LateUpdate()
    {
        Vector3 camPosition = mainCamera.transform.position;
        Vector3 lookDir = indicadorBalas.transform.position - camPosition;
        //lookDir.y = 0;
        indicadorBalas.transform.rotation = Quaternion.LookRotation(lookDir);
    }

    public void Disparar()
    {
        if (balasDisponibles > 0 && !bloqueoDeGestos && modoPistola && !powerUpActivo)
        {
            audioFuente.PlayOneShot(sonidoDisparo);
            GameObject bala;

            bala = Instantiate(balaPrefab, puntoInstanciadorDeBalas.transform.position, Quaternion.identity);

            Rigidbody rb = bala.GetComponent<Rigidbody>();
            rb.AddForce(puntoInstanciadorDeBalas.transform.forward * fuerzaProyectil, ForceMode.Impulse);

            Destroy(bala, 5);

            balasDisponibles--;
            indicadorBalas.text = balasDisponibles.ToString();
        }

    }

    public void CambiarModoPistola()
    {
        modoPistola = true;
        permitirRecarga = true;

        InvokeRepeating("Disparar", 0f, 1.5f);
        //Disparar();

        if (powerUpActivo)
        {
            CancelInvoke("SalirModoEspada");
            SalirModoEspada();
        }
    }

    public void CancelarDisparo()
    {
        CancelInvoke("Disparar");
        modoPistola = false;
    }

    public void Recargar ()
    {
        if (modoPistola && permitirRecarga)
        {
            audioFuente.PlayOneShot(sonidoReload);
            balasDisponibles = 20;
            permitirRecarga = false;
            bloqueoDeGestos = true;
            indicadorBalas.text = "R";
            Invoke("FinalizarRecarga", 1.5f);
        }
    }

    private void FinalizarRecarga()
    {
        bloqueoDeGestos = false;
        permitirRecarga = true;
        indicadorBalas.text = balasDisponibles.ToString();
    }

    public void CambiarModoEspada()
    {
        if (powerUpHabilitado && !bloqueoDeGestos)
        {
            espada.SetActive(true);
            modoPistola = false;
            permitirRecarga = false;
            powerUpActivo = true;

            powerUpHabilitado = false;
            indicadorBalas.gameObject.SetActive(false);


            if (tipoDeElemento == OpcionesDropdown.Fuego)
            {
                PowerUpControl.contPowerUpFuego = 0;
            }
            else if (tipoDeElemento == OpcionesDropdown.Agua)
            {
                PowerUpControl.contPowerUpAgua = 0;
            }

            Invoke("SalirModoEspada", 30f);
            audioPowerup.PlayOneShot(sonidoPowerActivo);
            powerupHaSonado = false;
        }
    }

    public void SalirModoEspada()
    {
        if (powerUpActivo)
        {
            espada.SetActive(false);
            indicadorBalas.gameObject.SetActive(true);

            powerUpActivo = false;
        }
    }





#if UNITY_EDITOR
    public void OnInspectorGUI()
    {
        tipoDeElemento = (OpcionesDropdown)EditorGUILayout.EnumPopup("Dropdown Options", tipoDeElemento);
    }
#endif 
}
