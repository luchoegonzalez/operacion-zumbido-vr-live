using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavesControl : MonoBehaviour
{
    [System.Serializable]
    public class Oleada
    {
        public int cantidadDeEnemigos;  
        public float intervaloDeSpawn;  
    }

    public Oleada[] oleadas;
    public List<GameObject> listaPrefabMosquitos;
    public GameObject destinoJugador;
    public float distanciaMinimaSpawnMosquitos = 10;
    public float tiempoEntreOleadas = 5f;

    private int olaActual = 0;
    public int cantidadOleadas = 0;
    private bool spawneandoOla = false;

    GameManager scriptGameManager;

    void Start()
    {
        scriptGameManager = gameObject.GetComponent<GameManager>();
    }

    public IEnumerator SpawnWave()
    {
        while (JugadorControl.saltando && scriptGameManager.vidaJugador > 0)
        {
            if (!spawneandoOla)
            {
                spawneandoOla = true;
                Oleada currentWave = (olaActual < oleadas.Length) ? oleadas[olaActual] : oleadas[oleadas.Length - 1];

                for (int i = 0; i < currentWave.cantidadDeEnemigos; i++)
                {
                    SpawnMosquitos();
                    yield return new WaitForSeconds(currentWave.intervaloDeSpawn);
                }

                if (olaActual < oleadas.Length - 1)
                {
                    olaActual++;
                }

                cantidadOleadas++;
                spawneandoOla = false;

                if (JugadorControl.saltando && scriptGameManager.vidaJugador > 0)
                {
                    yield return new WaitForSeconds(tiempoEntreOleadas);
                }
            }

            yield return null;
        }
    }

    void SpawnMosquitos()
    {
        int indiceAleatorio = Random.Range(0, listaPrefabMosquitos.Count);
        Instantiate(listaPrefabMosquitos[indiceAleatorio], PosicionRadomSpawn(), Quaternion.identity);
    }

    Vector3 PosicionRadomSpawn()
    {
        Vector3 PosicionDeAparicionRandom;
        float distancia;

        do
        {
            PosicionDeAparicionRandom = new Vector3(destinoJugador.transform.position.x + Random.Range(-20, 20), destinoJugador.transform.position.y, destinoJugador.transform.position.z + Random.Range(-20, 20));
            distancia = Vector3.Distance(PosicionDeAparicionRandom, destinoJugador.transform.position);

        } while (distancia < distanciaMinimaSpawnMosquitos);

        return PosicionDeAparicionRandom;
    }
}