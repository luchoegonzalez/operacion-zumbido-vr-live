using UnityEngine;

public class RotacionHelice : MonoBehaviour
{
    public float rapidezAnimacion;

    void Update()
    {
        transform.Rotate(0, 0, rapidezAnimacion * Time.deltaTime);
    }

}
