using UnityEngine;

public class BalaControl : MonoBehaviour
{
    public GameObject particulasExplosion;
    private void Update()
    {
        if (JugadorControl.saltando)
        {
            transform.Translate(Vector3.down * 2 * Time.deltaTime);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(particulasExplosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
