using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlesTutorialManos : MonoBehaviour
{
    public GameObject balaPrefab;
    public GameObject puntoInstanciadorDeBalas;
    public float fuerzaProyectil = 5;

    private void Start()
    {
        ChequearDisparo(true);
    }

    public void ChequearDisparo(bool disparar)
    {
        if (disparar)
        {
            CancelInvoke("Disparar");
            InvokeRepeating("Disparar", 0, 3f);
        } else
        {
            CancelInvoke("Disparar");
        }
    }

    void Disparar()
    {
        if (gameObject.activeInHierarchy)
        {
            GameObject bala;

            bala = Instantiate(balaPrefab, puntoInstanciadorDeBalas.transform.position, Quaternion.identity);
            bala.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            bala.transform.SetParent(transform);

            Rigidbody rb = bala.GetComponent<Rigidbody>();
            rb.AddForce(puntoInstanciadorDeBalas.transform.forward * fuerzaProyectil, ForceMode.Impulse);

            Destroy(bala, 3);
        }
    }
}
