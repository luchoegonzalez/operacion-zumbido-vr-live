# Operación Z.U.M.B.I.D.O. VR

:es: Juego de realidad virtual en el que usas tus manos para disparar a los mosquitos—sin necesidad de controles. Desarrollado en Unity para Meta Quest 2 y 3 como parte del laboratorio de innovación CAETI LIVE.

:us: Virtual reality game where you use your hands to shoot mosquitoes—no controllers needed. Developed in Unity for Meta Quest 2 and 3 as part of the CAETI LIVE innovation lab.

[Play on Itch.io](https://live-games.itch.io/operacion-zumbido)

### Gameplay
[![Watch the video](https://img.youtube.com/vi/43UPE8JSLLs/hqdefault.jpg)](https://youtu.be/43UPE8JSLLs)